#### Player with all skills available on Hi Scores, sorted by highest level first
```
$ rslevel -n "Noveah Lux" -o "level"

Woodcutting    68
Runecrafting   68
Crafting       64
Divination     63
Mining         58
Cooking        58
Magic          56
Fishing        55
Construction   52
Defence        51
Fletching      50
Summoning      49
Strength       48
Constitution   48
Attack         48
Smithing       47
Firemaking     47
Thieving       46
Agility        45
Prayer         44
Slayer         43
Hunter         43
Ranged         42
Farming        42
Herblore       41
Dungeoneering  41
---------------------
Overall        1318
```
##### osrs
```
$ rslevel -n "Noveah Nox" --osrs -o "level"

Cooking        52
Fishing        51
Attack         38
Agility        35
Strength       32
Constitution   28
Defence        23
Slayer         21
Prayer         18
Mining         18
Ranged         15
Firemaking     12
Woodcutting    11
Magic          10
Crafting       9
Thieving       NA
Smithing       NA
Runecrafting   NA
Hunter         NA
Herblore       NA
Fletching      NA
Farming        NA
Construction   NA
---------------------
Overall        381
```

#### Multiple players: levels below 15, all levels available, and a faulty name
_Note: Everything marked with * will be green in the terminal._
```
$ rslevel -n "Noveah Nox" -n "Noveah Lux" -n "example"

You asked for 3 players:
Skill           Noveah Nox   Noveah Lux   example      
Overall         621         *1318*        error        
Attack          31          *48*          error        
Defence         34          *51*          error        
Strength        31          *48*          error        
Constitution    30          *48*          error        
Ranged          NA          *42*          error        
Prayer          23          *44*          error        
Magic           33          *56*          error        
Cooking         25          *58*          error        
Woodcutting     33          *68*          error        
Fletching       NA          *50*          error        
Fishing         38          *55*          error        
Firemaking      30          *47*          error        
Crafting        32          *64*          error        
Smithing       *50*          47           error        
Mining          52          *58*          error        
Herblore        NA          *41*          error        
Agility         26          *45*          error        
Thieving        26          *46*          error        
Slayer          NA          *43*          error        
Farming         NA          *42*          error        
Runecrafting    45          *68*          error        
Hunter          15          *43*          error        
Construction    NA          *52*          error        
Summoning       NA          *49*          error        
Dungeoneering   NA          *41*          error        
Divination      21          *63*          error        
```
##### osrs
```
$ rslevel -n "Noveah Nox" -n "Animos" -n "example" --osrs

You asked for 3 players:
Skill           Noveah Nox   Animos       Noveah Lux   
Overall         381         *2277*        error        
Attack          38          *99*          error        
Defence         23          *99*          error        
Strength        32          *99*          error        
Constitution    28          *99*          error        
Ranged          15          *99*          error        
Prayer          18          *99*          error        
Magic           10          *99*          error        
Cooking         52          *99*          error        
Woodcutting     11          *99*          error        
Fletching       NA          *99*          error        
Fishing         51          *99*          error        
Firemaking      12          *99*          error        
Crafting        9           *99*          error        
Smithing        NA          *99*          error        
Mining          18          *99*          error        
Herblore        NA          *99*          error        
Agility         35          *99*          error        
Thieving        NA          *99*          error        
Slayer          21          *99*          error        
Farming         NA          *99*          error        
Runecrafting    NA          *99*          error        
Hunter          NA          *99*          error        
Construction    NA          *99*          error
```

#### Multiple players, only asking for Runecrafting level, showing levels only
```
$ rslevel -n "Noveah Nox" -n "Ledria Lux" -n "Noveah Lux" -n "example" -n "Ledria Nox" -s "Runecrafting" -l

You asked for 5 players:
Noveah Nox   Ledria Lux   Noveah Lux   example      Ledria Nox   
45           45          *68*          error        46          
``` 
##### osrs
```
$ rslevel -n "Noveah Nox" -n "Animos" -n "Noveah Lux" -n "So Flawless" --osrs -s "Runecrafting" -l

You asked for 4 players:
Noveah Nox    Animos        Noveah Lux    So Flawless   
NA           *99*           error         79
```
