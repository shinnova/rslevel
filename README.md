# rslevel
Get your Runescape levels or compare players' levels.


#### Dependencies
+ Python3
+ Colorama
+ urllib3

```
usage: rslevel [-h] -n NAMES [-s SKILL] [-l] [-o {skill,level}] [-r] [--osrs]

Check or compare your Runescape levels.

optional arguments:
  -h, --help            show this help message and exit
  -n NAMES, --name NAMES
                        Character name(s). Multiple names must be split over
                        multiple -n/--name.
  -s SKILL, --skill SKILL
                        What skill to show the level of. (default: all)
  -l                    Show levels only. Only works with -s/--skill set.
  -o {skill,level}, --order {skill,level}
                        1 name only: skill for alphabetical, level for
                        numerical ordering of skills. (default: skill)
  -r                    1 name only: When order is set to level, show skills
                        from lowest to highest.
  --osrs                Check osrs levels instead of rs3.
```
